<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dana_nasabah', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('nasabah_id');
            $table->unsignedInteger('toko_id');
            $table->integer('jumlah');
            $table->float('bunga', 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dana_nasabah');
    }
};
