<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detail_transaksi_gadai', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('transaksi_gadai_id');
            $table->foreign('transaksi_gadai_id')->on('transaksi_gadai')->references('id');

            $table->integer('cicilan_ke');
            $table->float('persen_bunga', 5, 2);
            $table->integer('jumlah_bunga');
            $table->integer('biaya_admin');
            $table->integer('total_biaya');
            $table->integer('total_pembayaran')->nullable();
            $table->integer('sisa_pinjaman')->nullable();
            $table->string('status');
            $table->date('tanggal_pelunasan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detail_transaksi_gadai');
    }
};
