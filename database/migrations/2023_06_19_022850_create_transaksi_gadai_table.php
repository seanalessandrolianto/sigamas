<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksi_gadai', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('nasabah_id');
            $table->unsignedInteger('toko_id');
            $table->float('gramasi', 5, 2);
            $table->integer('kadar');
            $table->integer('nilai_pinjaman');
            $table->date('tanggal_gadai');
            $table->integer('sisa_pinjaman')->nullable();
            $table->float('bunga', 5, 2);
            $table->string('foto_produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksi_gadai');
    }
};
