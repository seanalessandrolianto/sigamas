<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Status::create([
            'nama' => 'Terjual',
        ]);

        Status::create([
            'nama' => 'Terbeli',
        ]);

        Status::create([
            'nama' => 'Return',
        ]);

        Status::create([
            'nama' => 'Pencucian',
        ]);

        Status::create([
            'nama' => 'Reparasi',
        ]);

        Status::create([
            'nama' => 'Dalam Etalase',
        ]);

        Status::create([
            'nama' => 'Dalam Gudang',
        ]);
    }
}
