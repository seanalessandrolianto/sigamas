<?php

namespace Database\Seeders;

use App\Models\Lokasi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LokasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Lokasi::create([
            'nama' => 'Tukang Cuci',
        ]);

        Lokasi::create([
            'nama' => 'Tukang Reparasi',
        ]);

        Lokasi::create([
            'nama' => 'Etalase Toko',
        ]);

        Lokasi::create([
            'nama' => 'Gudang',
        ]);
    }
}
