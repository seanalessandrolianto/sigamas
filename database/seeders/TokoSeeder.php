<?php

namespace Database\Seeders;

use App\Models\Toko;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TokoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Toko::create([
            'nama' => 'Toko Alpha',
        ]);

        Toko::create([
            'nama' => 'Toko Beta',
        ]);

        Toko::create([
            'nama' => 'Toko Epsilon',
        ]);
    }
}
