<?php

namespace Database\Seeders;

use App\Models\Kadar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KadarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Kadar::create([
            'nama' => '750',
        ]);

        Kadar::create([
            'nama' => '700',
        ]);

        Kadar::create([
            'nama' => '420',
        ]);

        Kadar::create([
            'nama' => '375',
        ]);

        Kadar::create([
            'nama' => '300',
        ]);

    }
}
