<?php

namespace Database\Seeders;

use App\Models\Tipe;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Tipe::create([
            'nama' => 'Giwang',
        ]);

        Tipe::create([
            'nama' => 'Kalung',
        ]);

        Tipe::create([
            'nama' => 'Gelang',
        ]);

        Tipe::create([
            'nama' => 'Cincin',
        ]);
    }
}
