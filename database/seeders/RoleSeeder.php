<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create([
            'name' => 'owner',
        ]);

        Role::create([
            'name' => 'manager',
        ]);

        $owner = User::where('email', 'leonardo@omega.group')->first();
        $owner->assignRole('owner');

        $owner = User::where('email', 'feifei@omega.group')->first();
        $owner->assignRole('owner');

        $manager = User::where('email', 'manager@omega.group')->first();
        $manager->assignRole('manager');

    }
}
