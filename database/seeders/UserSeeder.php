<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $ownerLeo = User::create([
            'name' => 'Leonardo',
            'email' => 'leonardo@omega.group',
            'password' => Hash::make('password'),
        ]);

        $ownerLeo = User::create([
            'name' => 'Feifei',
            'email' => 'feifei@omega.group',
            'password' => Hash::make('password'),
        ]);

        $manager = User::create([
            'name' => 'Manager',
            'email' => 'manager@omega.group',
            'password' => Hash::make('password'),
        ]);


    }
}
