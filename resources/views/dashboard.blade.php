@extends('layout')

@section('content')
    @if (session()->has('id_toko'))
        <a href="{{ route('nasabah.index') }}" type="button" class="btn btn-primary btn-lg btn-block">Nasabah</a>
        <a href="{{ route('transaksi-gadai.index') }}" type="button" class="btn btn-primary btn-lg btn-block">Transaksi
            Gadai</a>
        <a href="{{ route('lupakan-toko') }}" type="button"
            class="btn btn-primary btn-lg btn-block">Pilih Toko</a>

        <hr>
        <div class="col-md-3">
            <div class="card bg-success text-white mb-3">
              <div class="card-body">
                <h3>Total Tagihan</h3>
                <p class="card-text">Rp. {{number_format($totalTagihan)}}</p>
              </div>
            </div>
          </div>
    @else
        <h3>Silahkan memilih toko untuk memulai!</h3>
        <hr>
        @foreach ($tokos as $toko)
            <a href="{{ route('pilih-toko', ['id_toko' => $toko->id]) }}" type="button"
                class="btn btn-primary btn-lg btn-block">{{ $toko->nama }}</a>
        @endforeach
    @endif
@endsection
