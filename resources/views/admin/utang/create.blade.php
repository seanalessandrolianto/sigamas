@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tambah Utang</h3>
                <form class="forms-sample" method="POST" action="{{route('utang.store')}}">
                    @csrf
                    <div class="form-group">
                      <label for="jumlah">Jumlah</label>
                      <input type="number" class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah">
                    </div>
                    <div class="form-group">
                        <label for="bunga">Bunga</label>
                        <input type="text" class="form-control" id="bunga" placeholder="Bunga" name="bunga">
                      </div>
                      <div class="form-group">
                        <label for="nama_bank">Nama Bank</label>
                        <input type="text" class="form-control" id="nama_bank" placeholder="Nama Bank" name="nama_bank">
                      </div>

                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection