@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Utang</h3>
                <a class="btn btn-primary" href="{{route('utang.create')}}">Tambah Utang</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Jumlah</th>
                            <th>Bunga</th>
                            <th>Bank</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($utangs as $utang)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$utang->jumlah}}</td>
                                <td>{{$utang->bunga}}</td>
                                <td>{{$utang->nama_bank}}</td>
                                <td><a href="{{route('utang.show-pelunasan', $utang->id)}}" class="btn btn-primary">Pelunasan</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection