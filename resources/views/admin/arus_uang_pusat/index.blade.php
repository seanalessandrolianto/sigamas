@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Arus Uang Pusat</h3>
                <a class="btn btn-primary" href="{{route('arus-uang-pusat.create')}}">Tambah Arus Uang Pusat</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Jumlah</th>
                            <th>Tipe</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($arus_uang_pusats as $arus_uang_pusat)
                            <tr>
                                <td>{{date('d M Y', strtotime($arus_uang_pusat->created_at))}}</td>
                                <td>Rp. {{number_format($arus_uang_pusat->jumlah)}}</td>
                                <td>{{$arus_uang_pusat->tipe}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection