@extends('layout')

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Tambah Arus Uang Pusat</h3>
                    <form class="forms-sample" method="POST" action="{{ route('arus-uang-pusat.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="jumlah">Jumlah Dana</label>
                            <input type="text" class="form-control" id="jumlah" placeholder="Jumlah"
                                name="jumlah">
                        </div>
                        <div class="form-group">
                            <label for="tipe">Tipe</label>
                            <select name="tipe" class="form-control">
                                <option value="Masuk">Masuk</option>
                                <option value="Keluar">Keluar</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
