@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tambah Utang Distibutor</h3>
                <form class="forms-sample" method="POST" action="{{route('utang-distributor.store')}}">
                    @csrf
                    <div class="form-group">
                      <label for="gramasi">Gramasi</label>
                      <input type="number" class="form-control" id="gramasi" placeholder="Gramasi" name="gramasi" step="0.01" min="0.01">
                    </div>
                    <div class="form-group">
                      <label for="deadline">Deadline</label>
                      <input type="date" class="form-control" id="deadline" placeholder="Deadline" name="deadline">
                    </div>
                    <div class="form-group">
                      <label for="nama_distributor">Nama Distributor</label>
                      <input type="text" class="form-control" id="nama_distributor" placeholder="Nama Distributor" name="nama_distributor">
                    </div>
                    
                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection