@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Pelunasan Utang Distributor</h3>
                <form class="forms-sample" method="POST" action="{{route('utang-distributor.store-pelunasan', $utang->id)}}">
                    @csrf
                    <div class="form-group">
                      <label for="gramasi">Gramasi</label>
                      <input type="text" class="form-control" id="gramasi" placeholder="Gramasi" name="gramasi" value="{{$utang->gramasi}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="deadline">Deadline</label>
                        <input type="text" class="form-control" id="deadline" placeholder="Deadline" name="deadline" value="{{$utang->deadline}}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="nama_distributor">Nama Distributor</label>
                        <input type="text" class="form-control" id="nama_distributor" placeholder="Nama Distributor" name="nama_distributor" value="{{$utang->nama_distributor}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="gramasi_pelunasan">Gramasi Pelunasan</label>
                        <input type="number" class="form-control" id="gramasi_pelunasan" placeholder="Gramasi" name="gramasi_pelunasan">
                    </div>

                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection