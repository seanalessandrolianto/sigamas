@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Utang Distributor</h3>
                <a class="btn btn-primary" href="{{route('utang-distributor.create')}}">Tambah Utang Distributor</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Jumlah (Gram)</th>
                            <th>Deadline</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($utangs as $utang)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$utang->gramasi}}</td>
                                <td>{{$utang->deadline}}</td>
                                <td><a href="{{route('utang-distributor.show-pelunasan', $utang->id)}}" class="btn btn-primary">Pelunasan</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection