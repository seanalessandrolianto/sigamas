@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Transfer</h3>
                <a class="btn btn-primary" href="{{route('transfer.create')}}">Tambah Transfer</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Bank</th>
                            <th>Nama Pemilik</th>
                            <th>Nama Customer</th>
                            <th>Jumlah</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transfers as $transfer)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$transfer->nama_bank}}</td>
                                <td>{{$transfer->nama_pemilik}}</td>
                                <td>{{$transfer->nama_customer}}</td>
                                <td>{{$transfer->jumlah}}</td>
                                <td>-</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection