@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tambah Transfer</h3>
                <form class="forms-sample" method="POST" action="{{route('transfer.store')}}">
                    @csrf
                    <div class="form-group">
                      <label for="nama_bank">Nama Bank</label>
                      <input type="text" class="form-control" id="nama_bank" placeholder="Nama Bank" name="nama_bank">
                    </div>
                    <div class="form-group">
                      <label for="nama_pemilik">Nama Pemilik</label>
                      <input type="text" class="form-control" id="nama_pemilik" placeholder="Nama Pemilik" name="nama_pemilik">
                    </div>
                    <div class="form-group">
                        <label for="nama_customer">Nama Customer</label>
                        <input type="text" class="form-control" id="nama_customer" placeholder="Nama Customer" name="nama_customer">
                      </div>
                      <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input type="text" class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah">
                      </div>

                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection