@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <form action="{{route('laporan-besar.detail-asset')}}">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="gramasi">Gramasi</label>
                                <input id="gramasi" type="number" step="0.01" class="form-control" name="gramasi">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="kadar">Filter dengan Kadar</label>
                                <select class="form-control" name="kadar" id="kadar">
                                  <option value="" selected>Pilih Kadar</option>
                                  @foreach ($kadar as $item)
                                      <option value="{{$item->nama}}">{{$item->nama}}</option>
                                  @endforeach
                                </select>
                              </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="tipe">Filter dengan Tipe</label>
                                <select class="form-control" id="tipe" name="tipe">
                                  <option value="" selected>Pilih Tipe</option>
                                  @foreach ($tipe as $item)
                                      <option value="{{$item->nama}}">{{$item->nama}}</option>
                                  @endforeach
                                </select>
                              </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="status">Filter dengan Status</label>
                                <select class="form-control" id="status" name="status">
                                  <option value="" selected>Pilih Status</option>
                                  @foreach ($status as $item)
                                      <option value="{{$item->nama}}">{{$item->nama}}</option>
                                  @endforeach
                                </select>
                              </div>
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Cari</button>
                    </form>
                </div>
                <h3 class="card-title">Tabel Barang Emas</h3>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Foto</th>
                        <th>Gramasi</th>
                        <th>Kadar</th>
                        <th>Tipe</th>
                        <th>Stok</th>
                        <th>Lokasi</th>
                        <th>Toko</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($barangEmases as $barangEmas)
                      <tr>
                        @if ($barangEmas->parent != null)
                            <td colspan="2">Child</td>
                        @else
                            <td>{{$barangEmas->nama}}</td>
                            <td> <img src="{{URL::to('/').'/'.$barangEmas->foto}}"> </td>
                        @endif
                        <td>{{$barangEmas->gramasi}}</td>
                        <td>{{$barangEmas->kadar}}</td>
                        <td>{{$barangEmas->tipe}}</td>
                        <td>{{$barangEmas->stok}} pcs</td>
                        <td>{{$barangEmas->lokasi}}</td>
                        <td>{{$barangEmas->toko}}</td>
                        <td>{{$barangEmas->status}}</td>
                        
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection