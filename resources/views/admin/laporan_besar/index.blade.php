@extends('layout')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="card bg-warning text-white mb-3">
          <div class="card-body">
            <h3>Asset Emas</h3>
            <p class="card-text">{{$assetEmas}} gr</p>
            <a href="{{route('laporan-besar.detail-asset')}}">Detail</a>
          </div>
        </div>
      </div>
  
      <div class="col-md-3">
        <div class="card bg-success text-white mb-3">
          <div class="card-body">
            <h3>Asset Uang</h3>
            <p class="card-text">$10,000</p>
          </div>
        </div>
      </div>
  
      <div class="col-md-3">
        <div class="card bg-info text-white mb-3">
          <div class="card-body">
            <h6 class="card-subtitle mb-2">Conversion Rate</h6>
            <p class="card-text">5%</p>
          </div>
        </div>
      </div>
  
      <div class="col-md-3">
        <div class="card bg-warning text-dark mb-3">
          <div class="card-body">
            <h6 class="card-subtitle mb-2">Page Views</h6>
            <p class="card-text">50,000</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
    
@endsection