@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Laporan Barang Emas</h3>
                <form action="{{route('barang-emas.buat-laporan')}}" method="GET">
                    @csrf
                    <div class="row mt-4">
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_awal">Tanggal Awal</label>
                                <input type="date" class="form-control" name="tanggal_awal" id="tanggal_awal">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_akhir">Tanggal Akhir</label>
                                <input type="date" class="form-control" name="tanggal_akhir" id="tanggal_akhir">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Cari</button>
                </form>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Tipe</th>
                        <th>Nilai</th>
                        <th>Jumlah</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($riwayats as $riwayat)
                      <tr>
                        <td>{{date('d M Y', strtotime($riwayat->created_at))}}</td>
                        <td>{{$riwayat->tipe}}</td>
                        <td>{{$riwayat->jumlah}} pcs</td>
                        <td>Rp. {{number_format($riwayat->nilai)}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                <a href="{{route('barang-emas.export-laporan')}}" class="btn btn-primary btn-sm mt-3">Export Laporan Ke Excel</a>

            </div>
        </div>
    </div>
</div>
@endsection