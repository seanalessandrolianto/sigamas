@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Pembelian (Return) Barang Emas</h3>
                <form class="forms-sample" method="POST" action="{{route('barang-emas.store-pembelian')}}">
                  @csrf
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <label for="filterTipe">Filter dengan Tipe</label>
                        <select class="form-control" id="filterTipe" name="filterTipe">
                          <option value="" selected>Pilih Tipe</option>
                          @foreach ($tipe as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <label for="filterKadar">Filter dengan Kadar</label>
                        <select class="form-control" name="filterKadar" id="filterKadar">
                          <option value="" selected>Pilih Kadar</option>
                          @foreach ($kadar as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group">
                      <input type="hidden" name="barang_emas_id" id="barang_emas_id">
                      <label for="kadar">Masukkan Nama</label>
                        <select class="form-control select2-input" type="text" id="name" name="name"></select>
                    </div>
                  </div>
                    <hr>
                    <p>Data di bawah ini akan terisi otomatis sesuai stok yang ada. Pastikan data ini sudah sesuai.</p>
                    <br>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" name="nama" readonly>
                    </div>
                    <div class="form-group">
                      <label for="tipe">Tipe</label>
                      <input type="text" class="form-control" id="tipe" name="tipe" readonly>
                    </div>
                    <div class="form-group">
                      <label for="gramasi">Gramasi</label>
                      <input type="text" class="form-control" id="gramasi" name="gramasi" readonly>
                    </div>
                      <div class="form-group">
                        <label for="kadar">Kadar</label>
                        <input type="text" class="form-control" id="kadar" name="kadar" readonly>
                      </div>
                      <div class="form-group">
                        <label for="lokasi">Lokasi</label>
                        <input type="text" class="form-control" id="lokasi" name="lokasi" readonly>
                      </div>
                      <div class="form-group">
                        <label for="toko">Toko</label>
                        <input type="text" class="form-control" id="toko" name="toko" readonly>
                      </div>
                      <div class="form-group">
                        <label for="status">Status</label>
                        <input type="text" class="form-control" id="status" name="status" readonly>
                      </div>
                      <div class="form-group">
                        <label for="catatan">Catatan (Tidak Wajib)</label>
                        <input type="text" class="form-control" id="catatan" placeholder="Catatan" name="catatan" readonly>
                      </div>
                      <hr>
                      <p>Masukkan keterangan transaksi</p>
                      <br>
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="harga_beli">Harga Beli</label>
                            <input type="text" class="form-control" id="harga_beli" placeholder="Harga Beli" name="harga_beli">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="jumlah">Jumlah</label>
                            <input type="text" class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="gramasi_beli">Gramasi</label>
                            <input type="text" class="form-control" id="gramasi_beli" placeholder="Jumlah" name="gramasi_beli">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="bukti_transaksi">Upload Bukti Transaksi</label>
                            <input type="file" class="form-control" id="bukti_transaksi" placeholder="Upload Bukti Transaksi" name="bukti_transaksi">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="catatan">Catatan Transaksi</label>
                            <input type="text" class="form-control" id="catatan" placeholder="Catatan" name="catatan">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="catatan">Kondisi</label>
                            <select name="kondisi" id="kondisi" class="form-control">
                              <option value="Bagus">Bagus</option>
                              <option value="Perlu Cuci">Perlu Cuci</option>
                              <option value="Perlu Reparasi">Perlu Reparasi</option>
                              <option value="Perlu Lebur">Perlu Lebur</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      
                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.4.slim.js" integrity="sha256-dWvV84T6BhzO4vG6gWhsWVKVoa4lVmLnpBOZh/CAHU4=" crossorigin="anonymous"></script>
<script>
  
  $(document).ready(function(){

    $('.select2-input').select2({
      ajax: {
        url :"{{route('barang-emas.cari')}}",
        delay: 250,
        data: function(params) {  
          var inputTipe = $('#filterTipe').val();
          var inputKadar = $('#filterKadar').val();
          var query = {
            search: params.term,
            tipe: inputTipe,
            kadar: inputKadar, 
          }

          return query;
        }
      }
    });
    
    $('.select2-input').on('select2:select', function(e){
      var data = e.params.data;
      var id = data['id'];
      fetchBarangEmasDataById(id);
      $('#barang_emas_id').val(id);

    });

    $('#tipe').on('change', function(){
      fetchGramasi($(this).val());
    });

    $('#gramasi').on('change', function(){
      fetchBarangEmasData($('#tipe').val(), $(this).val());
    });
  })
  
  function fetchGramasi(tipe) {
    $.ajax({
      url: "{{route('barang-emas.fetch-gramasi-by-tipe')}}",
      method: 'GET',
      data: {
        '_token' : '<?php echo csrf_token() ?>',
        tipe : tipe,
      },
      success: function(result) {
        console.log(result);
        var selectGramasi = $('#gramasi');
        selectGramasi.empty();

        var option = $('<option>', {
            value: 0,
            text: "Pilih Gramasi"
          });

        selectGramasi.append(option);
        $.each(result, function(index, item){
          var option = $('<option>', {
            value: item,
            text: item
          });

          selectGramasi.append(option);
        });
      }
    });
  }

  function fetchBarangEmasData(tipe, gramasi) {
    $.ajax({
      url: "{{route('barang-emas.fetch-barang-emas-data')}}",
      method: 'GET',
      data: {
        '_token' : '<?php echo csrf_token() ?>',
        tipe : tipe,
        gramasi : gramasi,
      },
      success: function(result) {
        $.each(result, function(index, item){
          $('#nama').val(item['nama']);
          $('#kadar').val(item['kadar']);
          $('#lokasi').val(item['lokasi']);
          $('#toko').val(item['toko']);
          $('#status').val(item['status']);
          $('#catatan').val(item['catatan']);
        });
      }
    });
  }

  function fetchBarangEmasDataById(id) {
    $.ajax({
      url: "{{route('barang-emas.fetch-barang-emas-data-by-id')}}",
      method: 'GET',
      data: {
        '_token' : '<?php echo csrf_token() ?>',
        id : id,
      },
      success: function(result) {
        console.log(result);
          $('#nama').val(result['nama']);
          $('#tipe').val(result['tipe']);
          $('#gramasi').val(result['gramasi']);
          $('#kadar').val(result['kadar']);
          $('#stok').val(result['stok']);
          $('#lokasi').val(result['lokasi']);
          $('#toko').val(result['toko']);
          $('#status').val(result['status']);
          $('#catatan').val(result['catatan']);

          var baseUrl = '{{ URL::to("/") }}';
          var concatUrl = baseUrl.concat("/"+result["foto"]);
          console.log(concatUrl);
          $('#foto').attr('src', concatUrl);
          $('#foto').show();
      }
    });
  }
</script>