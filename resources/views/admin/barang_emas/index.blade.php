@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tabel Barang Emas</h3>
                <a href="{{route('barang-emas.create')}}" class="btn btn-primary btn-sm">Tambah</a>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Foto</th>
                        <th>Gramasi</th>
                        <th>Kadar</th>
                        <th>Tipe</th>
                        <th>Stok</th>
                        <th>Lokasi</th>
                        <th>Toko</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($barangEmases as $barangEmas)
                      <tr>
                        <td>{{$barangEmas->nama}}</td>
                        <td> <img src="{{URL::to('/').'/'.$barangEmas->foto}}"> </td>
                        <td>{{$barangEmas->gramasi}}</td>
                        <td>{{$barangEmas->kadar}}</td>
                        <td>{{$barangEmas->tipe}}</td>
                        <td>{{$barangEmas->stok}} pcs</td>
                        <td>{{$barangEmas->lokasi}}</td>
                        <td>{{$barangEmas->toko}}</td>
                        <td>{{$barangEmas->status}}</td>
                        <td>
                          <a class="btn btn-primary" href="{{url("riwayat/$barangEmas->id")}}">Riwayat</a>
                          @if($barangEmas->parent != null)
                            @if ($barangEmas->lokasi == "Tukang Cuci")
                              <a class="btn btn-warning" href="{{url("barang-emas/gudang/$barangEmas->id")}}">Masukkan ke Gudang</a>
                            @elseif($barangEmas->lokasi == "Tukang Reparasi")
                              <a class="btn btn-warning" href="{{url("barang-emas/cuci/$barangEmas->id")}}">Cuci Barang</a>
                              <a class="btn btn-warning" href="{{url("barang-emas/gudang/$barangEmas->id")}}">Masukkan ke Gudang</a>
                            @elseif($barangEmas->lokasi == "Tempat Lebur")
                              <a class="btn btn-warning" href="{{url("barang-emas/cuci/$barangEmas->id")}}">Cuci Barang</a>
                              <a class="btn btn-danger" href="{{url("barang-emas/reparasi/$barangEmas->id")}}">Reparasi Barang</a>
                              <a class="btn btn-warning" href="{{url("barang-emas/gudang/$barangEmas->id")}}">Masukkan ke Gudang</a>
                              <a class="btn btn-warning" href="{{url("barang-emas/tarik-owner/$barangEmas->id")}}">Tarik ke Owner</a>
                            @endif
                          @endif
                          
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection