@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Riwayat Barang Emas</h3>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Tipe</th>
                        <th>Nilai</th>
                        <th>Jumlah</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($riwayats as $riwayat)
                      <tr>
                        <td>{{date('d M Y', strtotime($riwayat->created_at))}}</td>
                        <td>{{$riwayat->tipe}}</td>
                        <td>{{$riwayat->jumlah}} pcs</td>
                        <td>Rp. {{number_format($riwayat->nilai)}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection