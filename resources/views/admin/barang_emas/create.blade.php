@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tambah Barang Emas</h3>
                <form class="forms-sample" method="POST" action="{{route('barang-emas.store')}}" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama">
                    </div>
                    <div class="form-group">
                      <label for="gramasi">Gramasi</label>
                      <input step="0.01" type="number" class="form-control" id="gramasi" placeholder="Gramasi" name="gramasi">
                    </div>
                    <div class="form-group">
                        <label for="kadar">Kadar</label>
                        <select class="form-control" id="kadar" name="kadar">
                          <option value="">-- Pilih Kadar --</option>
                          @foreach ($kadar as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="tipe">Tipe</label>
                        <select class="form-control" id="tipe" name="tipe">
                          <option value="">-- Pilih Tipe --</option>
                          @foreach ($tipe as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="lokasi">Stok</label>
                        <input type="number" name="stok" class="form-control" min=1>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="lokasi">Lokasi</label>
                        <select class="form-control" id="lokasi" name="lokasi">
                          <option value="">-- Pilih Lokasi --</option>
                          @foreach ($lokasi as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="toko">Toko</label>
                        <select class="form-control" id="toko" name="toko">
                          <option value="">-- Pilih Toko --</option>
                          @foreach ($toko as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" id="status" name="status">
                          <option value="">-- Pilih Status --</option>
                          @foreach ($status as $item)
                              <option value="{{$item->nama}}">{{$item->nama}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="catatan">Catatan (Tidak Wajib)</label>
                        <input type="text" class="form-control" id="catatan" placeholder="Catatan" name="catatan">
                      </div>
                      <div class="form-group">
                        <label for="foto">Foto (Tidak Wajib)</label>
                        <input type="file" class="form-control" id="foto" placeholder="Catatan" name="foto">
                      </div>
                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection