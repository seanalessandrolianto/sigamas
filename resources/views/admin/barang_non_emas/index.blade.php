@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tabel Barang Non Emas</h3>
                <a href="{{route('barang-non-emas.create')}}" class="btn btn-primary btn-sm">Tambah</a>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Tipe</th>
                        <th>Stok</th>
                        <th>Lokasi</th>
                        <th>Toko</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($barangNonEmases as $barangNonEmas)
                      <tr>
                        <td>{{$barangNonEmas->nama}}</td>
                        <td>{{$barangNonEmas->tipe}}</td>
                        <td>{{$barangNonEmas->stok}} pcs</td>
                        <td>{{$barangNonEmas->lokasi}}</td>
                        <td>{{$barangNonEmas->toko}}</td>
                        <td>{{$barangNonEmas->status}}</td>
                        <td>
                          <a class="btn btn-primary" href="{{url("riwayat-non-emas/$barangNonEmas->id")}}">Riwayat</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection