@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Penjualan Barang Non Emas</h3>
                <form class="forms-sample" method="POST" action="{{route('barang-non-emas.store-penjualan')}}">
                  @csrf
                    <div class="row">
                      <div class="col-6">
                        <div class="form-group">
                          <label for="barang_id">1. Pilih Barang</label>
                          <select class="form-control" id="barang_id" name="barang_id">
                            <option value="0" selected>Pilih Barang</option>
                            @foreach ($barangNonEmases as $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <p>3. Data di bawah ini akan terisi otomatis sesuai stok yang ada. Pastikan data ini sudah sesuai.</p>
                    <br>
                    <div class="row">
                      <div class="col-6">
                          <div class="form-group">
                            <label for="tipe">Tipe</label>
                            <input type="text" class="form-control" id="tipe" name="tipe" readonly>
                          </div>
                          <div class="form-group">
                            <label for="stok">Stok</label>
                            <input type="text" class="form-control" id="stok" name="stok" readonly>
                          </div>
                          <div class="form-group">
                            <label for="lokasi">Lokasi</label>
                            <input type="text" class="form-control" id="lokasi" name="lokasi" readonly>
                          </div>
                          <div class="form-group">
                            <label for="toko">Toko</label>
                            <input type="text" class="form-control" id="toko" name="toko" readonly>
                          </div>
                          <div class="form-group">
                            <label for="status">Status</label>
                            <input type="text" class="form-control" id="status" name="status" readonly>
                          </div>
                          <div class="form-group">
                            <label for="catatan">Catatan (Tidak Wajib)</label>
                            <input type="text" class="form-control" id="catatan" placeholder="Catatan" name="catatan" readonly>
                          </div>
                      </div>
                      <div class="col-6">
                        <label for="">Foto Barang</label>
                        <img src="" alt="" id="foto">
                        <p>Tidak ada gambar</p>
                      </div>
                    </div>
                      <hr>
                      <p>4. Masukkan keterangan transaksi</p>
                      <br>
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <label for="harga_jual">Harga Jual</label>
                            <input type="text" class="form-control" id="harga_jual" placeholder="Harga Jual" name="harga_jual">
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <label for="jumlah">Jumlah</label>
                            <input type="text" class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <label for="bukti_transaksi">Upload Bukti Transaksi</label>
                            <input type="file" class="form-control" id="bukti_transaksi" placeholder="Upload Bukti Transaksi" name="bukti_transaksi">
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <label for="catatan">Catatan Transaksi</label>
                            <input type="text" class="form-control" id="catatan" placeholder="Catatan" name="catatan">
                          </div>
                        </div>
                      </div>
                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.4.slim.js" integrity="sha256-dWvV84T6BhzO4vG6gWhsWVKVoa4lVmLnpBOZh/CAHU4=" crossorigin="anonymous"></script>
<script>
  
  $(document).ready(function(){

    $('#barang_id').on('change', function(){
      fetchBarangNonEmasById($('#barang_id').val());
    });
  })
  

  function fetchBarangNonEmasById(barang_id) {
    $.ajax({
      url: "{{route('barang-non-emas.fetch-barang-non-emas-data')}}",
      method: 'GET',
      data: {
        '_token' : '<?php echo csrf_token() ?>',
        barang_id : barang_id,
      },
      success: function(item) {
          $('#tipe').val(item['tipe']);
          $('#stok').val(item['stok']);
          $('#lokasi').val(item['lokasi']);
          $('#toko').val(item['toko']);
          $('#status').val(item['status']);
          $('#catatan').val(item['catatan']);

         
      }
    });
  }
</script>