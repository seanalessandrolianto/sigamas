@extends('layout')

@section('content')
<div class="row">
    <?php
        //  dd(session()->get('id_toko')) 
         ?>
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Transaksi Gadai {{(isset($toko) ? "| Toko $toko->nama" : '')}}</h3>
                <a class="btn btn-primary" href="{{route('transaksi-gadai.create')}}">Transaksi Gadai Nasabah</a>
                <form action="{{route('transaksi-gadai.index')}}">
                    <div class="row mt-3">
                        <div class="col">
                            <input type="text" class="form-control" name="cari_nama" placeholder="Cari Nama...">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="cari_gramasi" placeholder="Cari Gramasi...">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="cari_kadar" placeholder="Cari Kadar...">
                        </div>
                    </div>
                    <button class="btn btn-secondary mt-1">Cari</button>
                </form>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal Mulai</th>
                            <th>Nama Nasabah</th>
                            <th>Foto Produk</th>
                            <th>Gramasi</th>
                            <th>Kadar</th>
                            <th>Nilai Pinjaman</th>
                            <th>Sisa Pinjaman</th>
                            <th>Bunga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksis as $transaksi)
                            <tr>
                                <td>{{date('d M Y', strtotime($transaksi->created_at))}}</td>
                                <td>{{$transaksi->nasabah->nama}}</td>
                                <td> <img src="{{URL::to('/').'/'.$transaksi->foto_produk}}"> </td>
                                <td>{{$transaksi->gramasi}} gr</td>
                                <td>{{$transaksi->kadar}} karat</td>
                                <td>Rp. {{number_format($transaksi->nilai_pinjaman)}}</td>
                                <td>
                                    @if ($transaksi->sisa_pinjaman >= 0)
                                    Rp. {{number_format($transaksi->sisa_pinjaman)}}
                                    @else
                                    LUNAS
                                    @endif
                                </td>
                                <td>{{$transaksi->bunga}} %</td>
                                <td>
                                    <a class="btn btn-primary" href="{{route('transaksi-gadai.show', $transaksi->id)}}">Detail</a>
                                    <button class="btn btn-danger" type="button" class="delete-btn" data-id-transaksi="{{$transaksi->id}}" data-bs-toggle="modal" data-bs-target="#deleteTransaksiModal">Hapus Transaksi</button>
                                    <div class="modal fade" id="deleteTransaksiModal" tabindex="-1" aria-labelledby="deleteTransaksiModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="deleteTransaksiModalLabel">
                                                        Hapus Transaksi
                                                    </h3>
                                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="{{route('transaksi-gadai.destroy', $transaksi->id)}}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="form-group">
                                                            <label for="email">Email Owner :</label>
                                                            <input type="password" class="form-control" id="email" name="email" placeholder="Email Owner">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password">Password Owner :</label>
                                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password Owner">
                                                        </div>
                                                        <button class="btn btn-danger" type="submit">Hapus Transaksi</button>
                                                    </form>
                                                </div>
                                                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Batal</button>
                                    
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

