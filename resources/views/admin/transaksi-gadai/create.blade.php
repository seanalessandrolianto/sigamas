@extends('layout')

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Tambah Transaksi Gadai</h3>
                    <form class="forms-sample" method="POST" action="{{ route('transaksi-gadai.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="toko_id">Toko</label>
                            <select name="toko_id" id="toko_id" class="form-control">
                                @foreach ($tokos as $toko)
                                    @if (session()->has('id_toko') && $toko->id == session()->get('id_toko'))
                                        <option value="{{ $toko->id }}" selected>{{ $toko->nama }}</option>
                                    @else
                                        <option value="{{ $toko->id }}">{{ $toko->nama }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nasabah_id">Nama</label>
                            <select name="nasabah_id" id="nasabah_id" class="form-control">
                                @foreach ($nasabahs as $nasabah)
                                    <option value="{{ $nasabah->id }}">{{ $nasabah->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if (auth()->user()->hasRole('owner'))
                            <div class="form-group">
                                <label for="tanggal_gadai">Tanggal Gadai</label>
                                <input type="date" class="form-control" id="tanggal_gadai" name="tanggal_gadai">
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="gramasi">Gramasi</label>
                            <input type="text" class="form-control" id="gramasi" placeholder="Gramasi" name="gramasi"
                                step="0.01" min="0.01">
                        </div>
                        <div class="form-group">
                            <label for="kadar">Kadar</label>
                            <input type="text" class="form-control" id="kadar" placeholder="Kadar" name="kadar">
                        </div>
                        <div class="form-group">
                            <label for="nilai_pinjaman">Nilai Pinjaman</label>
                            <input type="text" class="form-control" id="nilai_pinjaman" placeholder="Nilai Pinjaman"
                                name="nilai_pinjaman">
                        </div>

                        <div class="form-group">
                            <label for="bunga">Bunga</label>
                            <input type="text" class="form-control" id="bunga" placeholder="Bunga" name="bunga">
                        </div>

                        <div class="form-group">
                            <label for="foto_produk">Foto Produk</label>
                            <input type="file" class="form-control" id="foto_produk" placeholder="Foto Produk"
                                name="foto_produk">
                        </div>

                        <button type="submit" class="btn btn-primary me-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
