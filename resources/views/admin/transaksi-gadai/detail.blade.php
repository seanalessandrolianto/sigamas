@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Detail Transaksi Gadai</h3>
                <div class="row">
                    <div class="col-6">
                        <div class="row mt-2">
                            <div class="col-8">
                                <label>Nama Nasabah : </label>
                                <input type="text" value="{{$transaksiGadai->nasabah->nama}}" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label>Nilai Pinjaman : </label>
                                <input type="text" value="Rp. {{number_format($transaksiGadai->nilai_pinjaman)}}" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label>Sisa Pinjaman : </label>
                                <input type="text" value="Rp. {{number_format($transaksiGadai->sisa_pinjaman)}}" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row mt-2">
                            <div class="col-8">
                                <label>Gramasi : </label>
                                <input type="text" value="{{$transaksiGadai->gramasi}} gram" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label>Kadar : </label>
                                <input type="text" value="{{$transaksiGadai->kadar}} karat" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <table class="table mt-4">
                    <thead>
                        <tr>
                            <th>Cicilan Ke</th>
                            <th>Persen Bunga</th>
                            <th>Jumlah Bunga Bulanan</th>
                            <th>Total Biaya</th>
                            <th>Status</th>
                            <th>Sisa Pinjaman</th>
                            <th>Pembayaran</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksiGadai->detail as $detailTransaksi)
                            <tr>
                                <td>{{$detailTransaksi->cicilan_ke}}</td>
                                <td>{{$detailTransaksi->persen_bunga}} % (Rp. {{number_format($detailTransaksi->jumlah_bunga)}})</td>
                                <td>Rp. {{number_format($detailTransaksi->jumlah_bunga / 12)}}</td>
                                    @if($detailTransaksi->tanggal_pelunasan == null)
                                        <input type="hidden" value="{{$detailTransaksi->total_biaya}}" id="total_biaya_input">
                                    @endif
                                <td>
                                    Rp. {{number_format($detailTransaksi->jumlah_bunga / 12)}} (Bunga Bulanan) <br> Rp. {{number_format($detailTransaksi->sisa_pinjaman / 12)}} (Cicilan Bulanan) <br>
                                    = Rp. {{number_format($detailTransaksi->total_biaya)}}</td>
                                <td>{{$detailTransaksi->status}}</td>
                                <td>
                                    @if($detailTransaksi->sisa_pinjaman)
                                        Rp. {{number_format($detailTransaksi->sisa_pinjaman)}}</td>
                                    @else
                                    Rp. {{number_format($transaksiGadai->nilai_pinjaman)}}</td>
                                    @endif
                                <td>
                                    @if ($detailTransaksi->tanggal_pelunasan != null)
                                        {{$detailTransaksi->tanggal_pelunasan}} <br><br>
                                        Sebesar : Rp. {{number_format($detailTransaksi->total_pembayaran)}} <br><br>
                                        <a href="{{route('transaksi-gadai.download-nota', $detailTransaksi->id)}}">Cetak Nota</a>
                                    @elseif($detailTransaksi->sisa_pinjaman <= 0)
                                        Transaksi Selesai
                                    @else
                                        <form action="{{route('transaksi-gadai.pembayaran', $detailTransaksi->id)}}" method="POST">
                                            @csrf
                                            <input type="text" id="input-nominal" class="form-control mb-2" name="total_pembayaran" placeholder="Nominal Pembayaran" >
                                            {{-- <input type="hidden" id="input-total-pembayaran" name="total_pembayaran"> --}}
                                            <div>
                                                Nominal Pembayaran Minimal : Rp. {{number_format($detailTransaksi->total_biaya)}}
                                                {{-- <span>Total Pembayaran : </span>
                                                <span>{{number_format($detailTransaksi->total_biaya)}} + </span><span id="nominal_pembayaran_span">0</span> = <span id="total_pembayaran_span">0</span> --}}
                                            </div>
                                            <button type="submit" class="btn btn-primary mt-3">Pembayaran</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.6.4.slim.js" integrity="sha256-dWvV84T6BhzO4vG6gWhsWVKVoa4lVmLnpBOZh/CAHU4=" crossorigin="anonymous"></script>
<script>

    function hitungTotalPembayaran(event) {
        const total_biaya = $('#total_biaya_input').val();
        var total_pembayaran = parseInt(event.target.value) + parseInt(total_biaya);
        // $('#nominal_pembayaran_span').html(parseInt(event.target.value).toLocaleString());
        // $('#total_pembayaran_span').html(total_pembayaran.toLocaleString());
        // $('#input-total-pembayaran').val(total_pembayaran);
    }
</script>