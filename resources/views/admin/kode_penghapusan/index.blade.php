@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Kode Penghapusan</h3>
                <span>{{$kode}}</span>
            </div>
        </div>
    </div>
</div>
@endsection