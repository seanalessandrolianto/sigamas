@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Riwayat Gadai</h3>
                <div class="form-group">
                    <label>Nama : {{$nasabah->nama}}</label>
                    <br>
                    <label>Nomor Identitas : {{$nasabah->nomor_identitas}}</label>
                    <br>
                    <label>Telp : {{$nasabah->telp}}</label>
                    <br>
                    <label>Alamat : {{$nasabah->alamat}}</label>
                </div>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tanggal Gadai</th>
                            <th>Tanggal Pelunasan</th>
                            <th>Gramasi</th>
                            <th>Kadar</th>
                            <th>Nominal</th>
                            <th>Bunga</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($nasabah->transaksiGadai as $transaksi)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{date('d M Y', strtotime($transaksi->created_at))}}</td>
                                <td>{{date('d M Y', strtotime($transaksi->deadline))}}</td>
                                <td>{{ $transaksi->gramasi }} gr</td>
                                <td>{{ $transaksi->kadar }} karat</td>
                                <td>Rp. {{ number_format($transaksi->nilai_pinjaman) }}</td>
                                <td>{{ $transaksi->bunga }} %</td>
                            </tr>
                        @endforeach
                    </tbody>
            </div>
        </div>
    </div>
</div>
@endsection