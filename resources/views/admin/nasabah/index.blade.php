@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Nasabah</h3>
                <a class="btn btn-primary" href="{{route('nasabah.create')}}">Tambah Nasabah</a>
                <form action="{{route('nasabah.index')}}">
                    <div class="row mt-3">
                        <div class="col">
                            <input type="text" class="form-control" name="cari_nama" placeholder="Cari Nama...">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="cari_no_identitas" placeholder="Cari No. identitas...">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="cari_alamat" placeholder="Cari Alamat...">
                        </div>
                    </div>
                    <button class="btn btn-secondary mt-1">Cari</button>
                </form>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Nomor Identitas</th>
                            <th>Telp</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($nasabahs as $nasabah)
                            <tr>
                                <td>{{$nasabah->nama}}</td>
                                <td>{{$nasabah->nomor_identitas}}</td>
                                <td>{{$nasabah->telp}}</td>
                                <td>{{$nasabah->alamat}}</td>
                                <td>
                                    <a href="{{route('nasabah.riwayat-gadai', $nasabah->id)}}" class="btn btn-primary">Riwayat Gadai</a>
                                    <button class="btn btn-danger" type="button" class="delete-btn" data-id-nasabah="{{$nasabah->id}}" data-bs-toggle="modal" data-bs-target="#deleteNasabahModal">Hapus Nasabah</button>
                                    <div class="modal fade" id="deleteNasabahModal" tabindex="-1" aria-labelledby="deleteNasabahModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="deleteNasabahModalLabel">
                                                        Hapus Nasabah
                                                    </h3>
                                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="{{route('nasabah.destroy', $nasabah->id)}}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="form-group">
                                                            <label for="email">Email Owner :</label>
                                                            <input type="password" class="form-control" id="email" name="email" placeholder="Email Owner">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password">Password Owner :</label>
                                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password Owner">
                                                        </div>
                                                        <button class="btn btn-danger" type="submit">Hapus Nasabah</button>
                                                    </form>
                                                </div>
                                                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Batal</button>
                                    
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection