@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Tambah Nasabah</h3>
                <form class="forms-sample" method="POST" action="{{route('nasabah.store')}}">
                    @csrf
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="Nama Nasabah" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="nomor_identitas">Nomor Identitas</label>
                        <input type="text" class="form-control" id="nomor_identitas" placeholder="Nomor Identitas" name="nomor_identitas">
                      </div>
                      <div class="form-group">
                        <label for="telp">Telepon</label>
                        <input type="text" class="form-control" id="telp" placeholder="Telepon" name="telp">
                      </div>
                      <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Alamat" name="alamat">
                      </div>
                    
                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection