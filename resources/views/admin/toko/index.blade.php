@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Toko</h3>
                <a class="btn btn-primary" href="{{route('toko.create')}}">Tambah Toko</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Modal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tokos as $toko)
                            <tr>
                                <td>{{$toko->nama}}</td>
                                <td>Rp. {{number_format($toko->modal)}}</td>
                                <td><a href="{{route('toko.edit-modal', $toko->id)}}" class="btn btn-secondary">Edit Modal</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection