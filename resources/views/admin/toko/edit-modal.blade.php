@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Edit Modal</h3>
                <form class="forms-sample" method="POST" action="{{route('toko.update-modal', $toko->id)}}">
                    @csrf
                    <div class="form-group">
                      <label for="modal">Jumlah Modal yang Ingin Ditambah / Dikurangi</label>
                      <input type="text" class="form-control" id="modal" name="modal">
                    </div>
                    
                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection