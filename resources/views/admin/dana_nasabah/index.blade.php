@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Daftar Dana Nasabah</h3>
                <a class="btn btn-primary" href="{{route('dana-nasabah.create')}}">Tambah Dana Nasabah</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Jumlah</th>
                            <th>Bunga</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dana_nasabahs as $dana_nasabah)
                            <tr>
                                <td>{{$dana_nasabah->nasabah->nama}}</td>
                                <td>Rp. {{number_format($dana_nasabah->jumlah)}}</td>
                                <td>{{$dana_nasabah->bunga}} %</td>
                               
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection