@extends('layout')

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Tambah Dana Nasabah</h3>
                    <form class="forms-sample" method="POST" action="{{ route('dana-nasabah.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="toko_id">Toko</label>
                            <select name="toko_id" id="toko_id" class="form-control">
                                @foreach ($tokos as $toko)
                                    @if (session()->has('id_toko') && $toko->id == session()->get('id_toko'))
                                        <option value="{{ $toko->id }}" selected>{{ $toko->nama }}</option>
                                    @else
                                        <option value="{{ $toko->id }}">{{ $toko->nama }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nasabah_id">Nama</label>
                            <select name="nasabah_id" id="nasabah_id" class="form-control">
                                @foreach ($nasabahs as $nasabah)
                                    <option value="{{ $nasabah->id }}">{{ $nasabah->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_dana">Jumlah Dana</label>
                            <input type="text" class="form-control" id="jumlah_dana" placeholder="Jumlah Dana"
                                name="jumlah_dana">
                        </div>
                        <div class="form-group">
                            <label for="bunga">Bunga</label>
                            <input type="text" class="form-control" id="bunga" placeholder="Bunga" name="bunga">
                        </div>
                        <button type="submit" class="btn btn-primary me-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
