<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth')->group(function(){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/pilih-toko/{id_toko}', [App\Http\Controllers\HomeController::class, 'pilihToko'])->name('pilih-toko');
Route::get('/lupakan-toko/', [App\Http\Controllers\HomeController::class, 'lupakanSessionToko'])->name('lupakan-toko');
Route::get('/nasabah/{id}/riwayat-gadai', [App\Http\Controllers\Admin\NasabahController::class, 'showRiwayatGadai'])->name('nasabah.riwayat-gadai');
Route::post('/transaksi-gadai/{id_detail_transaksi}/pembayaran', [App\Http\Controllers\Admin\TransaksiGadaiController::class, 'pembayaran'])->name('transaksi-gadai.pembayaran');
Route::get('/transaksi-gadai/{id_detail_transaksi}/download-nota/', [App\Http\Controllers\Admin\TransaksiGadaiController::class, 'downloadNota'])->name('transaksi-gadai.download-nota');
Route::get('/kode_penghapusan', [App\Http\Controllers\Admin\KodePenghapusanController::class, 'index'])->name('kode_penghapusan.index');
Route::get('/toko/edit-modal/{id}', [App\Http\Controllers\Admin\TokoController::class, 'editModal'])->name('toko.edit-modal');
Route::post('/toko/update-modal/{id}', [App\Http\Controllers\Admin\TokoController::class, 'updateModal'])->name('toko.update-modal');

Route::resource('/nasabah', App\Http\Controllers\Admin\NasabahController::class);
Route::resource('/transaksi-gadai', App\Http\Controllers\Admin\TransaksiGadaiController::class);
Route::resource('/bunga', App\Http\Controllers\Admin\BungaController::class);
Route::resource('/toko', App\Http\Controllers\Admin\TokoController::class);
Route::resource('/dana-nasabah', App\Http\Controllers\Admin\DanaNasabahController::class);
Route::resource('/arus-uang-pusat', App\Http\Controllers\Admin\ArusUangPusatController::class);
