<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DetailTransaksiGadai;
use App\Models\Nasabah;
use App\Models\TransaksiGadai;
use Illuminate\Http\Request;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $nasabahs = new Nasabah();
        if($request->cari_nama)
        {
            $nasabahs = $nasabahs->where('nama', 'LIKE', '%'.$request->cari_nama.'%');
        }

        if($request->cari_no_identitas)
        {
            $nasabahs = $nasabahs->where('nomor_identitas', 'LIKE', '%'.$request->cari_no_identitas.'%');
        }

        if($request->cari_alamat)
        {
            $nasabahs = $nasabahs->where('alamat', 'LIKE', '%'.$request->cari_alamat.'%');
        }

        $nasabahs = $nasabahs->get();

        return view('admin.nasabah.index', compact('nasabahs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.nasabah.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newNasabah = new Nasabah();
        $newNasabah->nama = $request->nama;
        $newNasabah->nomor_identitas = $request->nomor_identitas;
        $newNasabah->telp = $request->telp;
        $newNasabah->alamat = $request->alamat;

        if($newNasabah->save()){
            return redirect()->back()->with('success', 'Berhasil menambah data baru!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function showRiwayatGadai($id)
    {
        $nasabah = Nasabah::with('transaksiGadai')->find($id);

        return view('admin.nasabah.riwayat-gadai', compact('nasabah'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $id)
    {
        $validasi = KodePenghapusanController::validasi($request->email, $request->password);
        if($validasi == true)
        {
            $transaksiGadais = TransaksiGadai::where('nasabah_id', $id)->get();
            foreach ($transaksiGadais as $transaksiGadai) {
                foreach ($transaksiGadai->detail as $detailTransaksiGadai) {
                    DetailTransaksiGadai::destroy($detailTransaksiGadai->id);
                }
                TransaksiGadai::destroy($transaksiGadai->id);
            }
            Nasabah::destroy($id);
            return redirect()->back()->with('success', 'Berhasil hapus nasabah');
        }
        else
        {
            return redirect()->back()->with('error', 'Kode owner tidak sesuai!');
        }
    }
}
