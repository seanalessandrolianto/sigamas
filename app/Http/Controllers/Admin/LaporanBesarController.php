<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BarangEmas;
use App\Models\Kadar;
use App\Models\Status;
use App\Models\Tipe;
use Illuminate\Http\Request;

class LaporanBesarController extends Controller
{
    public function index() {
        $assetEmas = BarangEmas::sum('gramasi');
        return view('admin.laporan_besar.index', compact('assetEmas'));
    }

    public function detailAsset(Request $request) {
        $kadar = Kadar::all();
        $status = Status::all();
        $tipe = Tipe::all();

        $barangEmases = new BarangEmas();
        $barangEmases = $barangEmases->where('created_at', '!=', null);
        
        if($request->kadar) {
            $barangEmases = $barangEmases->where('kadar', $request->kadar);
        }

        if($request->gramasi) {
            $barangEmases = $barangEmases->where('gramasi', $request->gramasi);
        }

        if($request->tipe) {

            $barangEmases = $barangEmases->where('tipe', $request->tipe);
        }

        if($request->status) {

            $barangEmases = $barangEmases->where('status', $request->status);
        }


        $barangEmases = $barangEmases->get();
        return view('admin.laporan_besar.detail_asset', compact('barangEmases', 'kadar', 'status', 'tipe'));
    }
}
