<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ArusUangPusat;
use Illuminate\Http\Request;

class ArusUangPusatController extends Controller
{
   /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $arus_uang_pusats = ArusUangPusat::all();
        return view('admin.arus_uang_pusat.index', compact('arus_uang_pusats'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.arus_uang_pusat.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newArusUangPusat = new ArusUangPusat();
        $newArusUangPusat->jumlah = $request->jumlah;
        $newArusUangPusat->tipe = $request->tipe;

        $newArusUangPusat->save();

        return redirect()->back()->with('success', 'Berhasil menambah data baru!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
