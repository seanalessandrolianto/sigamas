<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kadar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KadarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kadars = Kadar::all();
        return view('admin.kadar.index', compact('kadars'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.kadar.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $newKadar = new Kadar();
            $newKadar->nama = $request->nama;
            $newKadar->save();

            DB::commit();

            return redirect()->back()->with('success', 'Berhasil tambah kadar!');
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
