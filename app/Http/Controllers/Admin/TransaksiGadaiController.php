<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DetailTransaksiGadai;
use App\Models\Nasabah;
use App\Models\Toko;
use App\Models\TransaksiGadai;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf as DomPDF;

class TransaksiGadaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $id = $request->toko_id;
        $transaksis = new TransaksiGadai();

        if($request->cari_gramasi)
        {
            $transaksis = $transaksis->where('gramasi', $request->cari_gramasi);
        }

        if($request->cari_kadar)
        {
            $transaksis = $transaksis->where('kadar', $request->cari_kadar);
        }

        if($id != null)
        {
            $transaksis = $transaksis->where('toko_id', $id)->get();
            $toko = Toko::find($id);
            return view('admin.transaksi-gadai.index', compact('transaksis', 'toko'));
        }
        elseif(session()->has('id_toko'))
        {
            $transaksis = $transaksis->where('toko_id', session()->get('id_toko'))->get();
            return view('admin.transaksi-gadai.index', compact('transaksis'));
        }
        else 
        {
            $transaksis = $transaksis->get();
            return view('admin.transaksi-gadai.index', compact('transaksis'));
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $nasabahs = Nasabah::all();
        $tokos = Toko::all();
        return view('admin.transaksi-gadai.create', compact('nasabahs', 'tokos'));
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newTransaksiGadai = new TransaksiGadai();
        $newTransaksiGadai->nasabah_id = $request->nasabah_id;
        $newTransaksiGadai->toko_id = $request->toko_id;
        $newTransaksiGadai->gramasi = $request->gramasi;
        $newTransaksiGadai->kadar = $request->kadar;
        $newTransaksiGadai->nilai_pinjaman = $request->nilai_pinjaman;
        $newTransaksiGadai->sisa_pinjaman = $request->nilai_pinjaman;
        $newTransaksiGadai->bunga = $request->bunga;
        if(auth()->user()->hasRole('owner'))
        {
            $newTransaksiGadai->tanggal_gadai = $request->tanggal_gadai;
        } else {
            $newTransaksiGadai->tanggal_gadai = date('Y-m-d', strtotime(now()));
        }

        if(($request->file('foto_produk') != null))
        {
            $name = rand().time().'.'.$request->foto_produk->extension();
            $request->foto_produk->move(public_path('foto_produk'), $name);
            $newTransaksiGadai->foto_produk = "foto_produk/".$name;
        } else {
            return redirect()->back()->with('error', 'Mohon isi foto produk!');
        }

        $newTransaksiGadai->save();

        $newDetailTransaksiGadai = new DetailTransaksiGadai();
        $newDetailTransaksiGadai->transaksi_gadai_id = $newTransaksiGadai->id;
        $newDetailTransaksiGadai->cicilan_ke = 1;
        $newDetailTransaksiGadai->persen_bunga = $request->bunga;

        if($request->nilai_pinjaman >= 1000000) {
            $jumlahBunga = $request->bunga / 100 * $request->nilai_pinjaman;
        } else {
            $jumlahBunga = $request->bunga / 100 * 1000000;
        }
        $biayaAdmin = 0;
        $totalBiaya = $jumlahBunga / 12 + $newTransaksiGadai->nilai_pinjaman / 12 + $biayaAdmin;

        $newDetailTransaksiGadai->jumlah_bunga = $jumlahBunga;
        $newDetailTransaksiGadai->biaya_admin = $biayaAdmin;
        $newDetailTransaksiGadai->total_biaya = $totalBiaya;
        $newDetailTransaksiGadai->sisa_pinjaman = $newTransaksiGadai->nilai_pinjaman;
        $newDetailTransaksiGadai->status = 'BELUM TERBAYAR';
        $newDetailTransaksiGadai->save();

        return redirect()->back()->with('success', 'Berhasil menambah data baru!');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $transaksiGadai = TransaksiGadai::find($id);
        return view('admin.transaksi-gadai.detail', compact('transaksiGadai'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        $validasi = KodePenghapusanController::validasi($request->email, $request->password);
        if($validasi == true)
        {
            DetailTransaksiGadai::where('transaksi_gadai_id', $id)->delete();
            TransaksiGadai::destroy($id);
            return redirect()->back()->with('success', 'Berhasil hapus transaksi');
        }
        else
        {
            return redirect()->back()->with('error', 'Kode owner tidak sesuai!');
        }
    }

    public function pembayaran(Request $request, $id_detail_transaksi)
    {
        $detailTransaksiGadai = DetailTransaksiGadai::find($id_detail_transaksi);
        $detailTransaksiGadai->status = "TERBAYAR";
        $detailTransaksiGadai->tanggal_pelunasan = Carbon::now()->toDateString();
        $detailTransaksiGadai->total_pembayaran = $request->total_pembayaran;
        $detailTransaksiGadai->save();
        
        $sisaPinjaman = $detailTransaksiGadai->transaksiGadai->sisa_pinjaman - ($detailTransaksiGadai->total_pembayaran - $detailTransaksiGadai->jumlah_bunga / 12);

        $transaksiGadai = TransaksiGadai::find($detailTransaksiGadai->transaksiGadai->id);
        $transaksiGadai->sisa_pinjaman = $sisaPinjaman;
        $transaksiGadai->save();
        
        $newDetailTransaksiGadai = new DetailTransaksiGadai();
        $newDetailTransaksiGadai->transaksi_gadai_id = $transaksiGadai->id;
        $newDetailTransaksiGadai->cicilan_ke = $detailTransaksiGadai->cicilan_ke + 1;
        $newDetailTransaksiGadai->persen_bunga = $detailTransaksiGadai->persen_bunga;

        if($sisaPinjaman >= 1000000)
        {
            $jumlahBunga = $detailTransaksiGadai->persen_bunga / 100 * $sisaPinjaman;
        } else {
            $jumlahBunga = $detailTransaksiGadai->persen_bunga / 100 * 1000000;
        }

        $biayaAdmin = 0;
        $totalBiaya = $jumlahBunga / 12 + $sisaPinjaman / 12 + $biayaAdmin;

        $newDetailTransaksiGadai->jumlah_bunga = $jumlahBunga;
        $newDetailTransaksiGadai->biaya_admin = $biayaAdmin;
        $newDetailTransaksiGadai->total_biaya = $totalBiaya;
        $newDetailTransaksiGadai->sisa_pinjaman = $sisaPinjaman;
        $newDetailTransaksiGadai->status = 'BELUM TERBAYAR';
        $newDetailTransaksiGadai->save();

        return redirect()->back()->with('success', 'Berhasil melakukan pembayaran!');
    }

    public function downloadNota($id) {
        try {

            $detailTransaksiGadai = DetailTransaksiGadai::find($id)->toArray();
            // return view('admin.transaksi-gadai.template-nota', compact('detailTransaksiGadai'));
            $pdf = DomPDF::loadView('admin.transaksi-gadai.template-nota', compact('detailTransaksiGadai'));
            
            return $pdf->download('nota.pdf');
        } catch (\Exception $e) {
            dd($e);
            return redirect()->back()->with('error', 'Gagal print nota!');
        }
    }

}
