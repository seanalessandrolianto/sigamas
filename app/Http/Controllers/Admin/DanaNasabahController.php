<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DanaNasabah;
use App\Models\Nasabah;
use App\Models\Toko;
use Illuminate\Http\Request;

class DanaNasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dana_nasabahs = DanaNasabah::all();
        return view('admin.dana_nasabah.index', compact('dana_nasabahs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $nasabahs = Nasabah::all();
        $tokos = Toko::all();
        return view('admin.dana_nasabah.create', compact('nasabahs', 'tokos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newDanaNasabah = new DanaNasabah();
        $newDanaNasabah->nasabah_id = $request->nasabah_id;
        $newDanaNasabah->toko_id = $request->toko_id;
        $newDanaNasabah->jumlah = $request->jumlah_dana;
        $newDanaNasabah->bunga = $request->bunga;

        $newDanaNasabah->save();

        return redirect()->back()->with('success', 'Berhasil menambah data baru!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
