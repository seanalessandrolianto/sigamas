<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Utang;
use Illuminate\Http\Request;

class UtangController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $utangs = Utang::all();
        return view('admin.utang.index', compact('utangs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.utang.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newUtang = new Utang();
        $newUtang->jumlah = $request->jumlah;
        $newUtang->bunga = $request->bunga;
        $newUtang->nama_bank = $request->nama_bank;
        if($newUtang->save()){
            return redirect()->back()->with('success', 'Berhasil tambah utang!');
        }

        
    }

    public function showPelunasanPage($id) {
        $utang = Utang::find($id);
        return view('admin.utang.pelunasan', compact('utang'));
    }

    public function storePelunasan(Request $request, $id) {
        $utang = Utang::find($id);
        $utang->jumlah -= $request->jumlah_pelunasan;
        $utang->save();
        if($utang->save()){
            return redirect()->back()->with('success', 'Berhasil menambah pelunasan utang!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
