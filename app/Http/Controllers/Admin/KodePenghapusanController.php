<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class KodePenghapusanController extends Controller
{
    public function index()
    {
        $kode = self::generator();
        return view('admin.kode_penghapusan.index', compact('kode'));
    }

    public static function validasi($email, $password)
    {
        return (Auth::attempt(['email'=>$email, 'password'=>$password]));
    }

    public static function generator()
    {
        return substr(Crypt::encrypt(today()), -7);
    }
}
