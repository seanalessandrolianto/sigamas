<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Riwayat;
use Illuminate\Http\Request;

class RiwayatController extends Controller
{
    public function showRiwayat($barang_id) 
    {
        $riwayats = Riwayat::where('barang_id', $barang_id)->get();
        return view('admin.barang_emas.riwayat', compact('riwayats'));
    }
}
