<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BarangNonEmas;
use App\Models\Lokasi;
use App\Models\Riwayat;
use App\Models\RiwayatNonEmas;
use App\Models\Status;
use App\Models\Toko;
use Illuminate\Http\Request;

class BarangNonEmasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $barangNonEmases = BarangNonEmas::all();
        return view('admin.barang_non_emas.index', compact('barangNonEmases'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $lokasi = Lokasi::all();
        $status = Status::all();
        $toko = Toko::all();
        return view('admin.barang_non_emas.create', compact('lokasi', 'status', 'toko'));
    }

    public function createPenjualan()
    {
        $barangNonEmases = BarangNonEmas::all();
        $lokasi = Lokasi::all();
        $status = Status::all();
        $toko = Toko::all();
        return view('admin.barang_non_emas.transaksi.penjualan', compact('barangNonEmases', 'lokasi', 'status', 'toko'));
    }

    public function createPembelian()
    {
        
        $barangNonEmases = BarangNonEmas::all();
        $lokasi = Lokasi::all();
        $status = Status::all();
        $toko = Toko::all();
        return view('admin.barang_non_emas.transaksi.pembelian', compact('barangNonEmases', 'lokasi', 'status', 'toko'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $newBarangNonEmas = new BarangNonEmas();
        $newBarangNonEmas->nama = $request->nama;
        $newBarangNonEmas->tipe = $request->tipe;
        $newBarangNonEmas->stok = $request->stok;
        $newBarangNonEmas->lokasi = $request->lokasi;
        $newBarangNonEmas->toko = $request->toko;
        $newBarangNonEmas->catatan = $request->catatan;
        $newBarangNonEmas->status = $request->status;

        if(($request->file('foto') != null))
        {
            //upload  foto
            $name = $request->file('foto')->getClientOriginalName();
            $url = $request->file('foto')->store('public/foto_produk_emas');
            
            $newBarangNonEmas->foto = $url;

        }

        

        if($newBarangNonEmas->save()){
            $newRiwayat = new RiwayatNonEmas();
            $newRiwayat->barang_id = $newBarangNonEmas->id;
            $newRiwayat->nilai = 0;
            $newRiwayat->jumlah = $request->stok;
            $newRiwayat->tipe = "Penambahan Barang";
            if(isset($request->catatan)){
                $newRiwayat->catatan = $request->catatan;
            }
            $newRiwayat->save();

            return redirect()->back()->with('success', 'Berhasil menambah barang emas baru!');
        }
    }

    public function storePenjualan(Request $request)
    {
        $barangNonEmas = BarangNonEmas::find($request->barang_id);
        if($request->jumlah > $barangNonEmas->stok){
            return redirect()->back()->with('error', "Stok tidak mencukupi! Stok yang tersedia adalah $barangNonEmas->stok pcs");
        } elseif($request->jumlah == $barangNonEmas->stok) {
            $barangNonEmas->status = "Terjual Semua";
            $barangNonEmas->stok = $barangNonEmas->stok - $request->jumlah;
        } else {
            $barangNonEmas->status = "Terjual Parsial";
            $barangNonEmas->stok = $barangNonEmas->stok - $request->jumlah;
        }
        $barangNonEmas->save();
        
        //Harga jual (transaksi)
        $newRiwayat = new RiwayatNonEmas();
        $newRiwayat->barang_id = $barangNonEmas->id;
        $newRiwayat->nilai = $request->harga_jual;
            $newRiwayat->jumlah = $request->jumlah;
            $newRiwayat->tipe = "Penjualan";
        if(isset($request->catatan)){
            $newRiwayat->catatan = $request->catatan;
        }
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil tambah transaksi!');
    }

    public function storePembelian(Request $request)
    {
        $barangNonEmas = BarangNonEmas::find($request->barang_id);
        $barangNonEmas->status = "Terbeli";
        $barangNonEmas->stok += $request->jumlah;
        $barangNonEmas->save();
        
        //Harga beli (transaksi)
        $newRiwayat = new RiwayatNonEmas();
        $newRiwayat->barang_id = $barangNonEmas->id;
        $newRiwayat->nilai = $request->harga_beli;
            $newRiwayat->jumlah = $request->jumlah;
            $newRiwayat->tipe = "Pembelian";
        if(isset($request->catatan)){
            $newRiwayat->catatan = $request->catatan;
        }
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil tambah transaksi!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchBarangNonEmasData(Request $request){
        $barangNonEmas = BarangNonEmas::find($request->barang_id);
        return $barangNonEmas;
    }


}
