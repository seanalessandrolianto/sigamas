<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transfer;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $transfers = Transfer::all();
        return view('admin.transfer.index', compact('transfers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.transfer.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newTransfer = new Transfer();
        $newTransfer->nama_bank = $request->nama_bank;
        $newTransfer->nama_pemilik = $request->nama_pemilik;
        $newTransfer->nama_customer = $request->nama_customer;
        $newTransfer->jumlah = $request->jumlah;
        if(isset($request->keterangan))
        $newTransfer->keterangan = $request->keterangan;
        if($newTransfer->save()){
            return redirect()->back()->with('success', 'Berhasil tambah transfer!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
