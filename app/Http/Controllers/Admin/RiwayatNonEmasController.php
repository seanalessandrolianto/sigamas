<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RiwayatNonEmas;
use Illuminate\Http\Request;

class RiwayatNonEmasController extends Controller
{
    public function showRiwayatNonEmas($barang_id) 
    {
        $riwayats = RiwayatNonEmas::where('barang_id', $barang_id)->get();
        return view('admin.barang_non_emas.riwayat', compact('riwayats'));
    }
}
