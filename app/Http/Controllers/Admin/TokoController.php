<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Toko;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TokoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tokos = Toko::all();
        return view('admin.toko.index', compact('tokos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.toko.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $newToko = new Toko();
            $newToko->nama = $request->nama;
            $newToko->save();

            DB::commit();

            return redirect()->back()->with('success', 'Berhasil tambah toko!');
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $toko = Toko::find($id);
        return view('admin.toko.show', compact('toko'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function editModal(string $id)
    {
        $toko = Toko::find($id);
        return view('admin.toko.edit-modal', compact('toko'));
    }

    public function updateModal(Request $request, string $id)
    {
        $toko = Toko::find($id);
        $toko->modal += $request->modal;
        $toko->save();
        return redirect()->back()->with('success', 'Berhasil ubah modal!');
    }
}
