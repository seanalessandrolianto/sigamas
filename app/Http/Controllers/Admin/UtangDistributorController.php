<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UtangDistributor;
use Illuminate\Http\Request;

class UtangDistributorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $utangs = UtangDistributor::all();
        return view('admin.utang_distributor.index', compact('utangs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.utang_distributor.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $newUtang = new UtangDistributor();
        $newUtang->gramasi = $request->gramasi;
        $newUtang->deadline = $request->deadline;
        $newUtang->nama_distributor = $request->nama_distributor;
        if($newUtang->save()){
            return redirect()->back()->with('success', 'Berhasil tambah utang distributor!');
        }

        
    }

    public function showPelunasanPage($id) {
        $utang = UtangDistributor::find($id);
        return view('admin.utang_distributor.pelunasan', compact('utang'));
    }

    public function storePelunasan(Request $request, $id) {
        $utang = UtangDistributor::find($id);
        $utang->gramasi -= $request->gramasi_pelunasan;
        $utang->save();
        if($utang->save()){
            return redirect()->back()->with('success', 'Berhasil menambah pelunasan utang distributor!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
