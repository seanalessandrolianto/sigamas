<?php

namespace App\Http\Controllers\Admin;

use App\Exports\RiwayatExport;
use App\Http\Controllers\Controller;
use App\Models\BarangEmas;
use App\Models\Kadar;
use App\Models\Lokasi;
use App\Models\Riwayat;
use App\Models\Status;
use App\Models\Tipe;
use App\Models\Toko;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class BarangEmasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $barangEmases = BarangEmas::all();
        return view('admin.barang_emas.index', compact('barangEmases'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $kadar = Kadar::all();
        $lokasi = Lokasi::all();
        $status = Status::all();
        $tipe = Tipe::all();
        $toko = Toko::all();
        return view('admin.barang_emas.create', compact('kadar', 'lokasi', 'status', 'tipe', 'toko'));
    }

    public function createPenjualan()
    {
        $kadar = Kadar::all();
        $lokasi = Lokasi::all();
        $status = Status::all();
        $tipe = Tipe::all();
        $toko = Toko::all();
        return view('admin.barang_emas.transaksi.penjualan', compact('kadar', 'lokasi', 'status', 'tipe', 'toko'));
    }

    public function createPembelian()
    {
        $kadar = Kadar::all();
        $lokasi = Lokasi::all();
        $status = Status::all();
        $tipe = Tipe::all();
        $toko = Toko::all();
        return view('admin.barang_emas.transaksi.pembelian', compact('kadar', 'lokasi', 'status', 'tipe', 'toko'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        $newBarangEmas = new BarangEmas();
        $newBarangEmas->nama = $request->nama;
        $newBarangEmas->gramasi = $request->gramasi;
        $newBarangEmas->kadar = $request->kadar;
        $newBarangEmas->tipe = $request->tipe;
        $newBarangEmas->stok = $request->stok;
        $newBarangEmas->lokasi = $request->lokasi;
        $newBarangEmas->toko = $request->toko;
        $newBarangEmas->catatan = $request->catatan;
        $newBarangEmas->status = $request->status;

        if(($request->file('foto') != null))
        {
            //upload  foto
            $name = rand().time().'.'.$request->foto->extension();
            // Storage::disk('public')->put('foto_produk_emas/'.$name, $request->file('foto'));
            $request->foto->move(public_path('foto_produk_emas'), $name);

            $newBarangEmas->foto = "foto_produk_emas/".$name;
        }

        if($newBarangEmas->save()){
            $newRiwayat = new Riwayat();
            $newRiwayat->barang_id = $newBarangEmas->id;
            $newRiwayat->nilai = 0;
            $newRiwayat->jumlah = $request->stok;
            $newRiwayat->tipe = "Penambahan Barang";
            if(isset($request->catatan)){
                $newRiwayat->catatan = $request->catatan;
            }
            $newRiwayat->save();

            return redirect()->back()->with('success', 'Berhasil menambah barang emas baru!');
        }
    }

    public function storePenjualan(Request $request)
    {
        $barangEmas = BarangEmas::find($request->barang_emas_id);
        if($request->jumlah > $barangEmas->stok){
            return redirect()->back()->with('error', "Stok tidak mencukupi! Stok yang tersedia adalah $barangEmas->stok pcs");
        } elseif($request->jumlah == $barangEmas->stok) {
            $barangEmas->status = "Terjual Semua";
            $barangEmas->stok = $barangEmas->stok - $request->jumlah;
            $barangEmas->gramasi = $barangEmas->gramasi - $request->gramasi_jual;
        } else {
            $barangEmas->status = "Terjual Parsial";
            $barangEmas->stok = $barangEmas->stok - $request->jumlah;
            $barangEmas->gramasi = $barangEmas->gramasi - $request->gramasi_jual;
        }
        $barangEmas->save();
        
        $barangEmasTerjual = new BarangEmas();
        $barangEmasTerjual->gramasi = $request->gramasi_jual;
        $barangEmasTerjual->kadar = $request->kadar;
        $barangEmasTerjual->stok = $request->jumlah;
        $barangEmasTerjual->tipe = $request->tipe;
        $barangEmasTerjual->lokasi = "Customer";
        $barangEmasTerjual->toko = $request->toko;
        $barangEmasTerjual->status = "Terjual";
        $barangEmasTerjual->parent = $barangEmas->id;
        $barangEmasTerjual->save();

        //Harga jual (transaksi)
        $newRiwayat = new Riwayat();
        $newRiwayat->barang_id = $barangEmas->id;
        $newRiwayat->nilai = $request->harga_jual;
            $newRiwayat->jumlah = $request->jumlah;
            $newRiwayat->tipe = "Penjualan";
        if(isset($request->catatan)){
            $newRiwayat->catatan = $request->catatan;
        }
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil tambah transaksi!');
    }

    public function storePembelian(Request $request)
    {
        $barangEmas = BarangEmas::find($request->barang_emas_id);

        $barangEmas->status = "Terbeli";
        $barangEmas->stok += $request->jumlah;
        $barangEmas->save();
        
        $barangEmasTerbeli = new BarangEmas();
        $barangEmasTerbeli->gramasi = $request->gramasi_beli;
        $barangEmasTerbeli->kadar = $request->kadar;
        $barangEmasTerbeli->stok = $request->jumlah;
        $barangEmasTerbeli->tipe = $request->tipe;

        if ($request->kondisi == "Bagus") {
            $barangEmasTerbeli->lokasi = "Gudang";
            $barangEmasTerbeli->status = "Terbeli. Masuk ke gudang.";

        } elseif ($request->kondisi == "Perlu Cuci") {
            $barangEmasTerbeli->lokasi = "Tukang Cuci";
            $barangEmasTerbeli->status = "Terbeli. Masuk ke tukang cuci.";

        } elseif ($request->kondisi == "Perlu Reparasi") {
            $barangEmasTerbeli->lokasi = "Gudang";
            $barangEmasTerbeli->status = "Terbeli. Masuk ke tukang reparasi.";

        } elseif ($request->kondisi == "Perlu Lebur") {
            $barangEmasTerbeli->lokasi = "Gudang";
            $barangEmasTerbeli->status = "Terbeli. Masuk ke tempat lebur.";
        }
        
        $barangEmasTerbeli->toko = $request->toko;
        $barangEmasTerbeli->parent = $barangEmas->id;
        $barangEmasTerbeli->save();

        //Harga beli (transaksi)
        $newRiwayat = new Riwayat();
        $newRiwayat->barang_id = $barangEmas->id;
        $newRiwayat->nilai = $request->harga_beli;
            $newRiwayat->jumlah = $request->jumlah;
            $newRiwayat->tipe = "Pembelian";
        if(isset($request->catatan)){
            $newRiwayat->catatan = $request->catatan;
        }
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil tambah transaksi!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchGramasiByTipe(Request $request){
        $gramasies = BarangEmas::where('tipe', $request->tipe)->pluck('gramasi');
        return $gramasies;
    }
    public function fetchBarangEmasData(Request $request){
        $barangEmases = BarangEmas::where('tipe', $request->tipe)->where('gramasi', $request->gramasi)->get();
        return $barangEmases;
    }

    public function fetchBarangEmasDataById(Request $request){
        $barangEmases = BarangEmas::find($request->id);
        return $barangEmases;
    }

    public function showLaporanPage(Request $request) {
        if($request->get('tanggal_awal') != null && $request->get('tanggal_akhir') != null){
            $riwayats = Riwayat::whereBetween('created_at', [$request->get('tanggal_awal'), $request->get('tanggal_akhir')])->get();
        } else {
            $riwayats = Riwayat::all();
        }

        return view('admin.barang_emas.laporan', compact('riwayats'));
    }

    public function exportLaporan() {
        $riwayats = Riwayat::all();
        return Excel::download(new RiwayatExport, 'laporan.xlsx');
    }

    public function reparasiBarang($id) {
        $barangEmas = BarangEmas::find($id);
        $barangEmas->status = "Reparasi";
        $barangEmas->lokasi = "Tukang Reparasi";
        $barangEmas->save();

        $newRiwayat = new Riwayat();
        $newRiwayat->barang_id = $barangEmas->id;
        $newRiwayat->nilai = 0;
        $newRiwayat->jumlah = 0;
        $newRiwayat->tipe = "Reparasi";
        
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil mengubah status barang menjadi "Reparasi"');
    }

    public function cuciBarang($id) {
        $barangEmas = BarangEmas::find($id);
        $barangEmas->status = "Pencucian";
        $barangEmas->lokasi = "Tukang Cuci";
        $barangEmas->save();

        $newRiwayat = new Riwayat();
        $newRiwayat->barang_id = $barangEmas->id;
        $newRiwayat->nilai = 0;
        $newRiwayat->jumlah = 0;
        $newRiwayat->tipe = "Pencucian";
        
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil mengubah status barang menjadi "Pencucian"');
    }

    public function masukkanGudang($id) {
        $barangEmas = BarangEmas::find($id);
        $barangEmas->status = "Dalam Penyimpanan";
        $barangEmas->lokasi = "Gudang";
        $barangEmas->save();

        $newRiwayat = new Riwayat();
        $newRiwayat->barang_id = $barangEmas->id;
        $newRiwayat->nilai = 0;
        $newRiwayat->jumlah = 0;
        $newRiwayat->tipe = "Masuk ke Gudang";
        
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil mengubah status barang menjadi "Dalam Penyimpanan"');
    }

    public function tarikOwner($id) {
        $barangEmas = BarangEmas::find($id);
        $barangEmas->status = "Ditarik Owner";
        $barangEmas->lokasi = "Owner";
        $barangEmas->save();

        $newRiwayat = new Riwayat();
        $newRiwayat->barang_id = $barangEmas->id;
        $newRiwayat->nilai = 0;
        $newRiwayat->jumlah = 0;
        $newRiwayat->tipe = "Penarikkan oleh Owner";
        
        $newRiwayat->save();

        return redirect()->back()->with('success', 'Berhasil mengubah status barang menjadi "Ditarik Owner"');
    }

    public function cariBarangEmas(Request $request) {
       
        $barangEmas = new BarangEmas();
        if($request->search) {
            $barangEmas = $barangEmas->where('nama', 'like', '%'.$request->search.'%');
        }
        if($request->kadar){
            $barangEmas = $barangEmas->where('kadar', 'like', '%'.$request->kadar.'%');
        }
        if($request->tipe){
            $barangEmas = $barangEmas->where('tipe', 'like', '%'.$request->tipe.'%');
        }
        $barangEmas = $barangEmas->get();

        $arrResult = array();
            foreach ($barangEmas as $value) {
                $arrValue = [
                    'id' => $value->id,
                    'text' => $value->nama,
                ];
                $arrResult['results'][] = $arrValue;
            }
            
            return $arrResult;
    }
}
