<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaksiGadai;
use App\Models\Toko;
use App\Models\TransaksiGadai;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // if($request->id_toko)
        // {
            
        // }
        $tokos = Toko::all();
        
        if($request->session()->has('id_toko'))
        {
            $totalTagihan = 0;
            // dd($request->session()->pull('id_toko'));
            $transaksiGadais = TransaksiGadai::where('toko_id', $request->session()->get('id_toko'))->get();
            // dd($transaksiGadai);
            foreach ($transaksiGadais as $transaksiGadai) {
                foreach ($transaksiGadai->detail as $detailTransaksiGadai) {
                    if($detailTransaksiGadai->status == "BELUM TERBAYAR")
                    {
                        $totalTagihan += $detailTransaksiGadai->total_biaya;
                    }
                }
            }
            return view('dashboard', compact('tokos', 'totalTagihan'));

        }

        return view('dashboard', compact('tokos'));
    }

    public function pilihToko($id_toko)
    {
        session(['id_toko'=>$id_toko]);

        return redirect()->route('home');
    }

    public function lupakanSessionToko(Request $request)
    {
        $id_toko = $request->session()->pull('id_toko');
        return redirect()->route('home');
    }
}
