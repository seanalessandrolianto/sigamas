<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TransaksiGadai extends Model
{
    use HasFactory;

    protected $table = "transaksi_gadai";

    /**
     * Get the nasabah associated with the TransaksiGadai
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function nasabah(): HasOne
    {
        return $this->hasOne(Nasabah::class, 'id', 'nasabah_id');
    }

    public function detail()
    {
        return $this->hasMany(DetailTransaksiGadai::class, 'transaksi_gadai_id', 'id');
    }
}
