<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DanaNasabah extends Model
{
    use HasFactory;

    protected $table = "dana_nasabah";

    public function nasabah()
    {
        return $this->hasOne(Nasabah::class, 'id', 'nasabah_id');
    }
}
