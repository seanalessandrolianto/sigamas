<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangNonEmas extends Model
{
    use HasFactory;

    protected $table = "barang_non_emas";
}
