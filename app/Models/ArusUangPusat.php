<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArusUangPusat extends Model
{
    use HasFactory;

    protected $table = "arus_uang_pusat";
}
