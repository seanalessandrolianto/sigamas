<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailTransaksiGadai extends Model
{
    use HasFactory;

    protected $table = 'detail_transaksi_gadai';

    public function transaksiGadai()
    {
        return $this->hasOne(TransaksiGadai::class, 'id', 'transaksi_gadai_id');
    }
}
