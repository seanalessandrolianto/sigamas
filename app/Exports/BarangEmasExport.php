<?php

namespace App\Exports;

use App\Models\BarangEmas;
use Maatwebsite\Excel\Concerns\FromCollection;

class BarangEmasExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return BarangEmas::all();
    }
}
